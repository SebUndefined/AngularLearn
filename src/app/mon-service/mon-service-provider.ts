import {Injectable} from '@angular/core';
import { Film } from '../model/film.model';
import { Observable, observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class MonServiceProvider {
    // HttpClient
    private httpClient: HttpClient;

    /**
     * Constructor
     * @param httpClient
     */
    constructor(httpClient: HttpClient) {
        this.httpClient = httpClient;
     }

     /**
      * Return an Observable
      * @param titre
      * @returns Observable<Array<Film>>
      */
    rechercherFilm(titre: String): Observable<Array<Film>> {
        // Création de l'observable
        return new Observable(observer => {
            this.httpClient.get('https://swapi.co/api/films/?search=' + titre)
                .subscribe(data => {
                    if (data && data['results'].length > 0) {
                        observer.next(data['results']);
                    } else {
                        observer.error('Aucun film trouvé');
                    }
                    observer.complete();
                }
            );
        });
    }

    rechercherToutFilms(): Observable<Array<Film>> {
        return new Observable(observer => {
            this.httpClient.get('https://swapi.co/api/films/')
                .subscribe(data => {
                    if (data && data['results'].length > 0) {
                        observer.next(data['results']);
                    } else {
                        observer.error('Aucun film trouvé');
                    }
                    observer.complete();
                }
            );
        });
    }

    detailsFilm(id: String): Observable<Film> {
        return new Observable(observer => {
            this.httpClient.get<Film>('https://swapi.co/api/films/' + id)
            .subscribe(data => {
                if (data) {
                    observer.next(data);
                } else {
                    observer.error('Aucun film trouvé');
                }
                observer.complete();
            });
        });
    }
}
