import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { MonComposantComponent } from './mon-composant/mon-composant.component';
import { MonServiceProvider } from './mon-service/mon-service-provider';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { DetailsComponent } from './details/details.component';

// My routes
const routes: Routes = [
  {path: 'recherche', component: MonComposantComponent},
  {path: 'details/:id', component: DetailsComponent},
  {path: '', redirectTo: '/recherche', pathMatch: 'full'}
];


@NgModule({
  declarations: [
    AppComponent,
    MonComposantComponent,
    DetailsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    // For calling Rest API
    HttpClientModule,
    // For Routing
    RouterModule.forRoot(routes)
  ],
  providers: [
    MonServiceProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
