import { Component } from '@angular/core';
import { MonServiceProvider } from '../mon-service/mon-service-provider';


@Component({
    selector: 'app-mon-composant',
    templateUrl: './mon-composant.component.html',
    styleUrls: ['./mon-composant.component.css']
})
export class MonComposantComponent {

    private monService: MonServiceProvider;

    public films: Array<Object> = new Array<Object>();
    public recherche: String = '';
    public erreur: String = '';
    public loading: Boolean = false;

    constructor(monService: MonServiceProvider) {
        this.monService = monService;
    }

    rechercherFilm() {
        this.erreur = '';
        if (this.recherche !== '' && this.recherche.length >= 2) {
            this.loading = true;
            this.monService.rechercherFilm(this.recherche)
            .subscribe(
                value => this.films = value,
                error => this.erreur = error,
                () => this.loading = false
                );
        } else {
            this.erreur = 'Veuillez entrer au moins 2 caractères';
        }
    }

    toutRechercher() {
        this.loading = true;
        this.erreur = '';
        this.monService.rechercherToutFilms()
        .subscribe(
            value => this.films = value,
            error => this.erreur = error,
            () => this.loading = false
        );
    }
}
