export class Film {
    public episode_id: String;
    public title: String;
    public director: String;

    constructor(episode_id: String, title: String, director: String) {
        this.episode_id = episode_id;
        this.title = title;
        this.director = director;
    }
}
