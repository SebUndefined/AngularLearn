import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MonServiceProvider } from '../mon-service/mon-service-provider';
import { Film } from '../model/film.model';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  // ActivatedRoute: usefull for routing!!
  private activatedRoute: ActivatedRoute;
  private serviceProvider: MonServiceProvider;

  public episode_id: String = '';
  public film: Film;

  constructor(activatedRoute: ActivatedRoute, monService: MonServiceProvider) {
    this.activatedRoute = activatedRoute;
    this.serviceProvider = monService;
   }

  // Executed when the component is ready for presentation
  ngOnInit() {
    this.activatedRoute.params.subscribe(
      params => this.episode_id = params['id']
    );
    this.serviceProvider.detailsFilm(this.episode_id)
      .subscribe(
        value => this.film = value
      );

  }

}
